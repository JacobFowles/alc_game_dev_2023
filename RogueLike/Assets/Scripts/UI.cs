using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public GameObject[] hearts;
    public TextMeshProUGUI coinText;
    public GameObject keyIcon;
    public TextMeshProUGUI levelText;
    public RawImage map;



    public static UI instance;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateHealth(int health)
    {
        for(int i = 0; i < hearts.length; ++i)
        {
            hearts[i].SetActive(i < health);
        }
    }

    public void UpdateCoinText ( int coinAmount)
    {
        coinText.text = coins.ToString();
    }

    public void ToggleKeyIcon (bool toggle)
    {
        keyIcon.SetActive(toggle);

    }
}
