using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generation : MonoBehaviour
{
    [Header("Map Properties")]
    public int mapWidth = 5;
    public int mapHeight = 5;
    public int roomsToGenerate = 9;
    public float tileSize = .16f;
    public float roomSize = 12f;

    public int seed;
    private int roomCount;
    private bool roomsInstantiated;

    private Vector2 firstRoomPos;

    private bool[,] map;
    public GameObject roomPrefab;
    private List<Room> roomObjects = new List<Room>();

    //creating a Singleton
    public static Generation instance;

    public Vector2 generalDirection;

    void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        Random.InitState(seed);
        Generate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Generate()
    {
        //Create a new map of the specified size
        map = new bool[mapWidth, mapHeight];
        //Check to see if we can place a room in the center of the map 
        CheckRoom(3,3,0,Vector2.zero, true);
        InstantiateRooms();
        //find the player in the scene, and position them inside the first room.
        FindObjectOfType<PlayerController2D>().transform.position = firstRoomPos * roomSize * tileSize;
    }

    void CheckRoom(int x, int y, int remaining, Vector2 generalDirection, bool firstRoom = false)
    {

        //If we have generated all of the rooms that we need, stop checking the room
        if(roomCount >=roomsToGenerate)
            return;
        //If this is outside the bounds of the actual map, stop the function
        if(x < 0 || x > mapWidth-1  || y < 0 || y > mapHeight-1)
            return;
        //If this is not the first room, and there is no more room to check, stop the function
        if(firstRoom == false && remaining <= 0)
            return;
        //If the given map tile is already occupied, stop the function
        if(map[x,y] == true)
            return;
        // if this is the first room, store the room position
        if(firstRoom == true)
            firstRoomPos = new Vector2(x,y);
        // add one to roomCount and srt the map tile to be true
        roomCount++;
        map[x,y] = true;


        bool north = Random.value > (generalDirection == Vector2.up ? 0.2f : 0.08);
        bool south = Random.value > (generalDirection == Vector2.down ? 0.2f : 0.08);
        bool east = Random.value > (generalDirection == Vector2.right ? 0.2f : 0.08);
        bool west = Random.value > (generalDirection == Vector2.left ? 0.2f : 0.08);
        
        int maxRemaining = roomsToGenerate / 4;
        //if north is true, make a room one tile above the current
        if(north || firstRoom)
            CheckRoom(x,y + 1, firstRoom ? maxRemaining : remaining -1, firstRoom ? Vector2.up : generalDirection);
        if(south || firstRoom)
            CheckRoom(x,y - 1, firstRoom ? maxRemaining : remaining -1, firstRoom ? Vector2.down : generalDirection);
        if(east || firstRoom)
            CheckRoom(x + 1,y, firstRoom ? maxRemaining : remaining -1, firstRoom ? Vector2.right : generalDirection);
        if(west || firstRoom)
            CheckRoom(x - 1,y, firstRoom ? maxRemaining : remaining -1, firstRoom ? Vector2.left : generalDirection);


    }

    void InstantiateRooms()
    {
        if(roomsInstantiated)
            return;
        roomsInstantiated = true;

        for(int x = 0; x < mapWidth; ++x)
        {
            for(int y = 0; y < mapHeight; ++y)
            {
                if(map[x,y] == false)
                    continue;

                //instantiate a new room prefab
                GameObject roomObj = Instantiate(roomPrefab, new Vector3(x,y,0) * roomSize * tileSize, Quaternion.identity);
                //Get reference to the room script of the new room object
                Room room = roomObj.GetComponent<Room>();

                //if we're within the boundary of the map, and if there is room above us
                if(y< mapHeight - 1 && map[x,y+1] == true)
                {
                    //enable the north door and disable the north wall
                    room.northDoor.gameObject.SetActive(true);
                    room.northWall.gameObject.SetActive(false);
                }
                if(y> 0 && map[x,y-1] == true)
                {
                    //enable the north door and disable the north wall
                    room.southDoor.gameObject.SetActive(true);
                    room.southWall.gameObject.SetActive(false);
                }
                if(x< mapHeight - 1 && map[x + 1,y] == true)
                {
                    //enable the north door and disable the north wall
                    room.eastDoor.gameObject.SetActive(true);
                    room.eastWall.gameObject.SetActive(false);
                }
                if(x > 0 && map[x-1,y] == true)
                {
                    //enable the north door and disable the north wall
                    room.westDoor.gameObject.SetActive(true);
                    room.westWall.gameObject.SetActive(false);
                }

                //if this is not the first room, call GenerateInterior().
                if(firstRoomPos != new Vector2(x,y))
                    room.GenerateInterior();
                //add the room to the roomObjects list
                roomObjects.Add(room);
            }
        }

        //After looping through every element inside the map array, call CalculatekeyAndExit().
        CalculateKeyandExit();
    }

    void CalculateKeyandExit()
    {
        float maxDist = 0;
        Room a = null;
        Room b = null;
        
        foreach(Room aRoom in roomObjects)
        {
            foreach(Room bRoom in roomObjects)
            {
                // Compare each of the rooms to find out which pair is the furthest away
                float dist = Vector3.Distance(aRoom.transform.position, bRoom.transform.position);
                if(dist > maxDist)
                {
                    a = aRoom;
                    b = bRoom;
                    maxDist = dist;
                }
            }
            // once room A and room B are found, spawn in the key and the exitdoor
            a.SpawnPrefab(a.keyPrefab);
            b.SpawnPrefab(b.exitDoorPrefab);
        }
    }
}
