using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum PickupType
{
    Coin,
    Health,
    Soul
        
}

public class Pickup : MonoBehaviour
{
    public PickupType type;
    public int value = 1;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        //if the pickup object has collided with the player
        if(other.CompareTag("Player"))
        {
            if(type== PickupType.Coin)
            {}
        }
    }
}
