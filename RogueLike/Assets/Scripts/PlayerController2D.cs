using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerController2D : MonoBehaviour
{
    public int curHP;
    public int maxHP;
    public int coins;
    public int attackDamage = 10;
    public float delay;

    public bool hasKey;
    public float tileSize = 0.16f;

    public SpriteRenderer sr;

    Rigidbody2D rb;

    // layer to avoid (mask)
    public LayerMask moveLayerMask;

    void Move (Vector2 dir)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, tileSize, moveLayerMask);

        // if there is no moveLayerMask detected in front of the player
        if(hit.collider == null)
        {
            transform.position += new Vector3(dir.x * tileSize, dir.y * tileSize, 0);   

            // EnemyManager.instance.OnPlayerMove();
        }
    }

    public void OnMoveUp(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.up);
    }
    
    public void OnMoveDown(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.down);
    }
    public void OnMoveLeft(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.left);
    }
    public void OnMoveRight(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            Move(Vector2.right);
    }

    public void OnAttackUp(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.up);
        
    }
    public void OnAttackDown(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.down);
    }
    public void OnAttackLeft(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.left);
        
    }
    public void OnAttackRight(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.right);
        
    }

    public void TakeDamage(int damageToTake)
    {
        curHP -= damageToTake;
        StartCoroutine(DamageFlash());

        if(curHP <= 0)
            SceneManager.LoadScene(0);
    }
    IEnumerator DamageFlash()
    {
        //get a reference to the defaut sprite color
        Color defaultColor = sr.color;
        //set the color to white
        sr.color = Color.white;
        //wait for a period of time before changing color
        yield return new WaitForSeconds(delay);
        //set the color back to its original color
        sr.color = defaultColor;
    }

    void TryAttack(Vector2 dir)
    {
        //Ignore the layer
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, tileSize, 1 << 7);
        if(hit.collider != null)
        {
            hit.transform.GetComponent<Enemy>().TakeDamage(attackDamage);
        }
    }

    public bool AddHealth (int amount)
    {

    }

    public void Addcoins(int amount)
    {
        coins += amount;
        UI.instance.UpdateCoinText(coins);
    }
}
